# RLGL for Mobile #

Uses LibGDX framework ([libgdx.badlogicgames.com](libgdx.badlogicgames.com)) and Java. Written by Scott Blechman.

*Note: This project was originally written to experiment with making a minimal, single-screen application. This led to current encapsulation issues that will be refactored to properly separate the application components.*