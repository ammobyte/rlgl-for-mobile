package io.ammobyte.rlgl;

import java.util.Random;

/**
 * Tracks elapsed time for functions e.g. state switching, time needed for input
 *
 * @author ammobyte
 */
public class Timer {

    enum T_STATE{
        STOPPED,
        ACTIVE
    }

    T_STATE state;

    private float seconds;

    Random r;

    public Timer(){
        state = T_STATE.STOPPED;
        r = new Random();
    }

    public void start(float s){
        state = T_STATE.ACTIVE;
        seconds = s;
    }

    public void start(float begin, float end){
        state = T_STATE.ACTIVE;
        seconds = r.nextFloat() * (end-begin)+begin;
    }

    public void stop(){
        state = T_STATE.STOPPED;
        seconds = 0;
    }

    public void update(float delta){
        if(seconds>0)
            seconds-=delta;
    }

    public float getSeconds(){
        return seconds;
    }
}
