package io.ammobyte.rlgl;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.util.Arrays;
import java.util.Random;

/**
 * Main game class, contains all game logic outside of the timer
 *
 * @author ammobyte
 */
public class RLGL extends ApplicationAdapter {

    //state of main game
    enum GAME_STATE{
        GO,
        STOP,
        START,
        OVER
    }
    GAME_STATE state;

    //used to switch to pause menu
    enum ACTIVITY{
        ACTIVE,
        PAUSED
    }
    ACTIVITY activity;

    //game timers

    Timer timer;            //counts down to the next change of state from go to stop (or vice versa)
    Timer goTimer;          //resets when device tapped during GO, measures how long a player can go without providing input before losing
    Timer stopTimer;        //counts delay before STOP allowances are counted
    Timer overDelayTimer;   //gives a small break before restarting the game
                            //e.g. a player is rapidly tapping and skips the game over state entirely

    Random r = new Random();

    int stopCounter = 0;    //used to count "allowances" during STOP (described below)

    final int STOP_BUFFER = 0;  //number of "allowances" for accidentally touching during STOP
    float goBuffer = 0.75f;    //number of seconds GO can occur without a touch before moving to OVER
    final float GB_FLOOR = 0.25f;   //lowest goBuffer can go to
    final int[] increaseNums = {13, 21, 34, 55, 89, 144, 233, 377, 610, 987};

    BitmapFont font;
    BitmapFont fontSmall;
    BitmapFont fancyFont;
    BitmapFont fancyFontSmall;
    SpriteBatch batch;

    int taps;   //taps per game, used to determine score

    Preferences prefs;  //holds high score and tutorial completion information

    Color pauseColor;   //dynamically determined based on active game state

    final Color RED     = new Color(1f, (float)109/255, (float)109/255, 1f);
    final Color GREEN   = new Color((float)109/255, 1f, (float)129/255, 1f);
    final Color YELLOW  = new Color(1f, (float)242/255, (float)109/255, 1f);
    //final Color BLUE    = new Color((float) 109 / 255, (float) 189 / 255, (float) 255 / 255, 1f);

    boolean highScoreThisRound = false; //checks if a special text needs to be rendered at the OVER screen

    Texture titleTexture;
    final float TITLE_POS_OFFSET = 50f;   //period of title sinusoidal animation
    float titlePosOffset = 0f;    //changeable offset for title position (when animating)
    float titleY;   //dynamically determined at instantiation
    int titleTicks = 0; //number of ticks for calculation of sinusoidal movement

    //responsive tap
    int beforeTaps = 0; //set to "taps" immediately before moving to GO from STOP
    int afterTaps = 0;  //set to "taps" immediately before moving to STOP from GO
    float avgTapTime;    //average time the user spends between taps (in seconds)
    int beforeTimerSeconds = 0; //time of the GO interval used to measure average

    boolean displayTutorial = false;    //toggles tutorial message display on START

    //long timeElapsed;   //time since last input, used to calculate GO delay
    //long startTime;
    float inputBuffer;  //how long after GO input until input can be used again
    float elapsedTime;

    final int MAX_SCORE = 999;

    /**
     * Instantiate all game values not already defined globally.
     */
    @Override
    public void create () {
        state = GAME_STATE.START;
        activity = ACTIVITY.ACTIVE;
        timer = new Timer();
        goTimer = new Timer();
        overDelayTimer = new Timer();
        stopTimer = new Timer();

        //set font sizes
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Roboto-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Gdx.graphics.getWidth()/15;
        font = generator.generateFont(parameter);
        parameter.size = Gdx.graphics.getWidth() / 23;
        fontSmall = generator.generateFont(parameter);

        generator = new  FreeTypeFontGenerator(Gdx.files.internal("Roboto-Light.ttf"));
        parameter.size = Gdx.graphics.getWidth()/15;
        fancyFont = generator.generateFont(parameter);
        parameter.size = Gdx.graphics.getWidth() / 23;
        fancyFontSmall = generator.generateFont(parameter);

        generator.dispose();
        batch = new SpriteBatch();

        prefs = Gdx.app.getPreferences("rlgl-vals");    //refer to "rlgl-vals" file on system for storage

        //check if the tutorial has already been seen
        if(!prefs.getBoolean("tut_complete", false)){
            displayTutorial = true;
        }

        //instantiate pauseColor before determining color
        pauseColor = new Color(0, 0, 0, 1f);

        taps = 0;

        titleTexture = new Texture(Gdx.files.internal("title.png"));

        //timeElapsed = 0;
        inputBuffer = goBuffer/6f;
        elapsedTime = 0;
    }

    /**
     * Contains all rendering and game logic.
     */
    @Override
    public void render () {
        //larger switch statement between game active or game paused defined by ACTIVITY
        switch(activity) {
            case ACTIVE:
                //smaller switch statement between game states defined by GAME_STATE
                switch (state) {
                    case START:
                        Gdx.gl.glClearColor((float)200/255, (float)200/255, (float)200/255, 1f);
                        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                        //input response, move out from START state to other
                        if (Gdx.input.justTouched()) {
                            //mark tutorial as complete if not already
                            if(!prefs.getBoolean("tut_complete", false)) {
                                prefs.putBoolean("tut_complete", true);
                                prefs.flush();
                            }
                            //initial timers always have state time of 5s and delay time of 1s
                            timer.start(5f);
                            //int i = r.nextInt(2);
                            int i = 0;
                            if (i == 0) {
                                goTimer.start(goBuffer);
                                //startTime = System.currentTimeMillis();
                                elapsedTime = 0;
                                state = GAME_STATE.GO;
                            } else if (i == 1)
                                state = GAME_STATE.STOP;
                                stopTimer.start(0.5f);
                        }
                        //sinusoidal movement on logo
                        titleTicks++;
                        titlePosOffset = TITLE_POS_OFFSET * ((float)Math.sin(titleTicks * .5 * Math.PI / 100));

                        //start screen rendering
                        batch.begin();
                        batch.draw(titleTexture, Gdx.graphics.getWidth() / 2 - (titleTexture.getWidth() / 2.75f), titleY + titlePosOffset, titleTexture.getWidth() * .75f, titleTexture.getHeight() * .75f);
                        font.setColor(55 / 255f, 55 / 255f, 55 / 255f, 1f);
                        fontSmall.setColor(55 /255f, 55 /255f, 55 / 255f, 1f);
                        fancyFont.setColor(55 / 255f, 55 / 255f, 55 / 255f, 1f);
                        fancyFontSmall.setColor(55 /255f, 55 /255f, 55 / 255f, 1f);

                        //title screen text if tutorial
                        if(displayTutorial){
                            font.setColor(110 / 255f, 110 / 255f, 110 / 255f, 1f);
                            font.draw(batch, "How to play:", (Gdx.graphics.getWidth() / 2) - (font.getBounds("How to play:").width / 2), (Gdx.graphics.getHeight() / 2));
                            font.draw(batch, "GREEN = GO, tap a lot!", (Gdx.graphics.getWidth() / 2) - (font.getBounds("GREEN = GO, tap a lot!").width / 2), (Gdx.graphics.getHeight() / 2) - (font.getBounds("How to play:").height)*1.5f);
                            font.draw(batch, "RED = STOP, don't touch!", (Gdx.graphics.getWidth() / 2) - (font.getBounds("RED = STOP, don't touch!").width / 2), (Gdx.graphics.getHeight() / 2) - (font.getBounds("GREEN = GO, tap a lot!").height)*3f);

                            font.setColor(55 / 255f, 55 / 255f, 55 / 255f, 1f);
                            font.draw(batch, "Tap to start...", (Gdx.graphics.getWidth() / 2) - (font.getBounds("Tap to start...").width / 2), (Gdx.graphics.getHeight() / 2) - (font.getBounds("RED = STOP, don't touch!").height*6f));
                        }
                        //or without tutorial
                        else{
                            font.draw(batch, "Tap to start...", (Gdx.graphics.getWidth() / 2) - (font.getBounds("Tap to start...").width / 2), Gdx.graphics.getHeight() / 2);
                        }

                        fancyFontSmall.draw(batch, "by            ", (Gdx.graphics.getWidth() / 2) - (fontSmall.getBounds("by   @ammobyte").width / 2), Gdx.graphics.getHeight()/27);
                        fancyFontSmall.setColor((float) 109 / 255, (float) 189 / 255, (float) 255 / 255, 1f);
                        fancyFontSmall.draw(batch, "     @ammobyte", (Gdx.graphics.getWidth() / 2) - (fontSmall.getBounds("by   @ammobyte").width / 2), Gdx.graphics.getHeight()/27);

                        fontSmall.setColor(Color.WHITE);
                        font.setColor(Color.WHITE);
                        fancyFontSmall.setColor(Color.WHITE);
                        fancyFont.setColor(Color.WHITE);
                        batch.end();
                        break;
                    case GO:
                        Gdx.gl.glClearColor((float)109/255, 1f, (float)129/255, 1f);
                        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                        stopCounter = 0;

                        if (Gdx.input.justTouched() && elapsedTime>=inputBuffer /*timeElapsed>0*/) {
                            goTimer.start(goBuffer);
                            if(taps<MAX_SCORE)
                            taps++;
                            //noinspection SuspiciousMethodCalls
                            if(Arrays.asList(increaseNums).contains(taps) && (goBuffer/2f>=GB_FLOOR)){
                                goBuffer/=2f;
                                inputBuffer = goBuffer/6f;
                            }
                            //startTime = System.currentTimeMillis();
                            elapsedTime = 0;
                        }

                        goTimer.update(Gdx.graphics.getDeltaTime());
                        if (goTimer.getSeconds() <= 0) {
                            Gdx.input.vibrate(500);
                            overDelayTimer.start(1);
                            goTimer.stop();
                            state = GAME_STATE.OVER;
                        }

                        timer.update(Gdx.graphics.getDeltaTime());
                        if (timer.getSeconds() <= 0) {
                            timer.start(2, 5);
                            goTimer.stop();

                            //add responsiveness to stop delay
                            afterTaps = taps;
                            avgTapTime = beforeTimerSeconds/(float)(afterTaps-beforeTaps);
                            stopTimer.start((goBuffer-avgTapTime)/2f);
                            state = GAME_STATE.STOP;
                        }

                        batch.begin();
                        if (taps > prefs.getInteger("num_taps", 0)) {
                            font.setColor(YELLOW);
                            font.draw(batch, "Score: " + taps, (Gdx.graphics.getWidth() / 2) - (font.getBounds("Score: " + taps).width / 2), (Gdx.graphics.getHeight()) - (Gdx.graphics.getWidth() / 12));
                        }
                        else {
                            font.setColor(Color.WHITE);
                            font.draw(batch, "Score: " + taps, (Gdx.graphics.getWidth() / 2) - (font.getBounds("Score: " + taps).width / 2), (Gdx.graphics.getHeight()) - (Gdx.graphics.getWidth() / 12));
                        }
                        batch.end();

                        //timeElapsed = (System.currentTimeMillis() - startTime) / 100;
                        //System.out.println(timeElapsed);
                        elapsedTime+=Gdx.graphics.getDeltaTime();
                        //System.out.println(elapsedTime+", "+inputBuffer);
                        break;
                    case STOP:
                        Gdx.gl.glClearColor(1f, (float)109/255, (float)109/255, 1f);
                        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                        if (Gdx.input.justTouched() && stopTimer.getSeconds() <= 0.0f) {
                            stopCounter++;
                        }

                        stopTimer.update(Gdx.graphics.getDeltaTime());
                        if(stopTimer.getSeconds() <= 0.0f)
                            stopTimer.getSeconds();

                        if (stopCounter > STOP_BUFFER) {
                            Gdx.input.vibrate(500);
                            overDelayTimer.start(1);
                            goTimer.stop();
                            state = GAME_STATE.OVER;
                        }

                        timer.update(Gdx.graphics.getDeltaTime());
                        if (timer.getSeconds() <= 0) {
                            stopTimer.stop();

                            timer.start(2, 6);
                            beforeTimerSeconds = (int)timer.getSeconds();
                            goTimer.start(goBuffer);
                            beforeTaps = taps;
                            //startTime = System.currentTimeMillis();
                            elapsedTime = 0;
                            state = GAME_STATE.GO;
                        }

                        batch.begin();
                        if (taps > prefs.getInteger("num_taps", 0)) {
                            font.setColor(YELLOW);
                            font.draw(batch, "Score: " + taps, (Gdx.graphics.getWidth() / 2) - (font.getBounds("Score: " + taps).width / 2), (Gdx.graphics.getHeight()) - (Gdx.graphics.getWidth() / 12));
                        }
                        else {
                            font.setColor(Color.WHITE);
                            font.draw(batch, "Score: " + taps, (Gdx.graphics.getWidth() / 2) - (font.getBounds("Score: " + taps).width / 2), (Gdx.graphics.getHeight()) - (Gdx.graphics.getWidth() / 12));
                        }
                        batch.end();
                        break;
                    case OVER:
                        Gdx.gl.glClearColor((float)109/255, (float)189/255, (float)255/255, 1f);
                        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                        stopCounter = 0;
                        timer.stop();
                        goTimer.stop();

                        //record high score change, if any
                        if (taps > prefs.getInteger("num_taps", 0)) {
                            prefs.putInteger("num_taps", taps);
                            highScoreThisRound = true;
                            prefs.flush();
                        }

                        //reset game if tapped after input delay
                        if (Gdx.input.justTouched() && overDelayTimer.getSeconds()<=0f) {
                            timer.start(5f);
                            taps = 0;
                            highScoreThisRound = false;
                            int i = r.nextInt(1 + 1);
                            if (i == 0) {
                                goTimer.start(1);
                                state = GAME_STATE.GO;
                                //startTime = System.currentTimeMillis();
                                elapsedTime = 0;
                            } else if (i == 1)
                                state = GAME_STATE.STOP;
                        }

                        overDelayTimer.update(Gdx.graphics.getDeltaTime());

                        batch.begin();
                        if (highScoreThisRound) {
                            fancyFont.setColor(YELLOW);
                            fancyFont.draw(batch, "High score!", (Gdx.graphics.getWidth() / 2) - (font.getBounds("High score!").width / 2), (Gdx.graphics.getHeight() / 2) + font.getBounds("GAME OVER").height*4.5f);
                            fancyFont.setColor(Color.WHITE);
                            font.setColor(Color.WHITE);
                        }
                        font.setColor(Color.WHITE);
                        font.draw(batch, "GAME OVER", (Gdx.graphics.getWidth() / 2) - (font.getBounds("GAME OVER").width / 2), Gdx.graphics.getHeight() / 2);
                        fancyFont.draw(batch, "Your score: " + taps, (Gdx.graphics.getWidth() / 2) - (font.getBounds("Your score: " + taps).width / 2), (Gdx.graphics.getHeight() / 2) - (font.getBounds("GAME OVER").height*3f));
                        fancyFont.draw(batch, "Best: " + prefs.getInteger("num_taps", 0), (Gdx.graphics.getWidth() / 2) - (font.getBounds("Best: " + prefs.getInteger("num_taps", 0)).width / 2), (Gdx.graphics.getHeight() / 2) - (font.getBounds("GAME OVER").height*4.5f));
                        font.draw(batch, "Tap to restart...", (Gdx.graphics.getWidth() / 2) - (font.getBounds("Tap to restart...").width / 2), (Gdx.graphics.getHeight() / 2) - (font.getBounds("Your score: "+taps).height)*7.5f);

                        batch.end();
                        break;
                }
                break;
            case PAUSED:
                Gdx.gl.glClearColor(pauseColor.r, pauseColor.g, pauseColor.b, 1);
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                if(Gdx.input.justTouched())
                    activity = ACTIVITY.ACTIVE;
                batch.begin();
                font.setColor(Color.WHITE);
                font.draw(batch, "PAUSED", (Gdx.graphics.getWidth() / 2) - (font.getBounds("PAUSED").width / 2), Gdx.graphics.getHeight() / 2);
                font.draw(batch, "Tap to resume...", (Gdx.graphics.getWidth() / 2) - (font.getBounds("Tap to resume...").width / 2), (Gdx.graphics.getHeight() / 2) - 100);
                batch.end();
        }
    }

    /**
     * Dispose any SpriteBatch, BitmapFont, or Texture
     */
    @Override
    public void dispose(){
        batch.dispose();
        font.dispose();
        fontSmall.dispose();
        titleTexture.dispose();

    }

    /**
     * Activity paused, set appropriate game state and dynamically set color based on active state (STOP or GO)
     */
    @Override
    public void pause(){
        //set pause menu color based on current game state
        pauseColor = new Color(0f, 0f, 0f, 1f);
        if(state == GAME_STATE.GO) {
            pauseColor.r = GREEN.r;
            pauseColor.g = GREEN.g;
            pauseColor.b = GREEN.b;
        }
        else if(state == GAME_STATE.STOP){
            pauseColor.r = RED.r;
            pauseColor.g = RED.g;
            pauseColor.b = RED.b;
        }
        else if(state == GAME_STATE.START || state == GAME_STATE.OVER){
            pauseColor.r = (float)45/255;
            pauseColor.g = (float)45/255;
            pauseColor.b = (float)45/255;
        }

        //move to pause menu if in a game (STOP or GO is currently active)
        if(state==GAME_STATE.GO||state==GAME_STATE.STOP)
            activity = ACTIVITY.PAUSED;
    }

    /**
     * Called at activity start, set UI elements based on device size
     * @param width device width
     * @param height    device height
     */
    @Override
    public void resize(int width, int height){
        titleY = Gdx.graphics.getHeight()-(Gdx.graphics.getHeight()/3);
    }
}
